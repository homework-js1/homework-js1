/** 1) Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості
значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.**/

let name = 'Nastya';
let admin = name;
console.log(admin);

/**2) Оголосити змінну days і ініціалізувати її числом від 1 до 10.
 Перетворіть це число на кількість секунд і виведіть на консоль.**/

const days = Math.floor(Math.random() * 10) + 1;
let sec = days * 24 * 60 * 60;
console.log(`Days: ${days}, seconds: ${sec}.`);

/** 3) Запитайте у користувача якесь значення і виведіть його в консоль. **/

const userName = prompt("What is your name?");
const userAge = +prompt("How old are your?");
console.log(`Hello ${userName}! Your age ${userAge}.`);

